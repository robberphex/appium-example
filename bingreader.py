import unittest

from appium import webdriver


class BingReaderTests(unittest.TestCase):
    def setUp(self):
        desired_caps = {
            'platformName': 'Android',
            'platformVersion': '6.0',
            'deviceName': 'Android Emulator',
            "appPackage": "com.microsoft.bingreader",
            "appActivity": ".MainActivity",
        }

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def tearDown(self):
        self.driver.quit()

    def test_hello(self):
        print("hello, world!")


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(BingReaderTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
